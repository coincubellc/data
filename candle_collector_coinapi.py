#!/usr/bin/env python3
from common import *
import requests

table_class = Candles1hLocal

log = set_logger(name=__name__, sysloglevel=None, emaillevel=None)

_base_url = 'https://rest.coinapi.io/v1/ohlcv'
headers = {'X-CoinAPI-Key': '22ABBCDD-5077-4D58-AB82-9986DC08940B'}


def get_last(ep: ExPair):
    """ Returns the last row for the ExPair, or None if nonexistent.
    """
    last = (table_class.query
            .filter(table_class.ex_pair_id == ep.id)
            .order_by(table_class.timestamp.desc())
            .first())

    return last


def collect_candles_coinapi():
    log.info("Gathering market data from Coinapi.io.")
    if not table_class.__table__.exists(engine):
        table_class.__table__.create(engine)

    ex_pairs = ExPair.query.filter(
        or_(
            ExPair.exchange.has(name='Kraken'),
            ExPair.exchange.has(name='HitBTC'),
            ExPair.exchange.has(name='Bitstamp'),
            ExPair.exchange.has(name='Binance'),
            ExPair.exchange.has(name='Kucoin'),
            ExPair.exchange.has(name='Liquid'),
        ),
        ExPair.active == True,
    ).order_by(ExPair.id).all()

    h_delta = pd.Timedelta('1h')
    log.info(f'{len(ex_pairs)} symbols to download.')
    retries = 0
    for ep in ex_pairs:
        # avoid getting stuck in an infinite loop if the exchange is down
        # and we keep getting ConnectionError
        if retries > 10:
            break
        if not ep.active:
            # Skip inactive ex_pairs
            continue

        try:
            last = get_last(ep)
            if last:
                start = last.timestamp + h_delta
                close = last.close
            else:
                start = datetime.utcnow() - pd.Timedelta(days=30)
                start = start.replace(hour=0, minute=0, second=0, microsecond=0)

                close = None
            if last is not None and datetime.utcnow() - last.timestamp < h_delta * 2:
                log.info(f"{ep} up to date: {last.timestamp}")
            else:
                log.info(f"Updating pair: {ep}. Last timestamp: {None if last is None else last.timestamp}")

            while datetime.utcnow() - start > h_delta:

                log.info(start.isoformat())
                symbol_id = f'{ep.exchange.name.upper()}_SPOT_{ep.base_currency.symbol}_{ep.quote_currency.symbol}'
                url = f'{_base_url}/{symbol_id}/history?period_id=1HRS&time_start={start.isoformat()}'

                r = requests.get(url, headers=headers)

                if r.status_code != 200:
                    if r.status_code == 400:
                        log.info('Missing asset')
                        log.debug(r.status_code)
                        log.debug(r.text)
                        break
                    log.info('Unable to reach CoinAPI...')
                    retries += 1
                    log.debug(r.status_code)
                    log.debug(r.text)
                    break

                h = pd.DataFrame(r.json())

                h = h.rename(index=str, 
                    columns={
                        'time_period_start': 'timestamp', 
                        'volume_traded': 'volume',
                        'price_close': 'close',
                        'price_high': 'high',
                        'price_low': 'low',
                        'price_open': 'open'
                        })
                h.index = pd.to_datetime(h['timestamp'], format='%Y-%m-%dT%H:%M:%S.0000000Z')
                h = h.sort_index()
                if not len(h):
                    break

                h = h[:-1]
                h = h.truncate(before=start)
                h = h[['open', 'close', 'high', 'low', 'volume']]
                h['ex_pair_id'] = ep.id

                if not len(h):
                    break

                h.reset_index(inplace=True)
                h['timestamp'] = h.timestamp.apply(lambda d: pd.to_datetime(str(d)))
                log.info(f'New 1H candles: {len(h)}. {h.timestamp.iloc[0]} - {h.timestamp.iloc[-1]}')

                h.to_sql(table_class.__tablename__, engine,
                         index=False, if_exists='append', chunksize=10000,
                         schema='coincube')
                db_session.commit()

                if len(h) <= 1:
                    # incomplete or up to date
                    break
                start = h.timestamp.iloc[-1] + h_delta
                close = h.close.iloc[-1]
        except Exception as e:
            log.exception('%s: Unhandled exception' % ep)

    log.info("Finished update.")


if __name__ == '__main__':
    collect_candles_coinapi()
