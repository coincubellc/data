import functools
from typing import Tuple, List

import pandas as pd
from datetime import timedelta
from sqlalchemy.exc import IntegrityError
import numpy as np

from analysis import find_candle_errors, find_gaps
from common import read_df, set_logger, engine, and_
from database import Candles1hCCompare, Candles1hLocal, ExPair, CombinedExPair1h, db_session

default_table = Candles1hCCompare
filler_table = Candles1hLocal
target_table = CombinedExPair1h

drop_on_errors = ['zero_or_less', 'null', 'inf']

log = set_logger(name=__name__, sysloglevel=None, emaillevel=None)


def fill_gaps(primary: pd.DataFrame, secondary: pd.DataFrame):
    """ Finds gaps in `primary` and fills them, if possible,
    using candles from `secondary`.
    """
    diff, _ = find_gaps(primary)
    if diff is None:
        return primary, None
    overlap = secondary.loc[secondary.index & diff]
    merged = primary.append(overlap)
    merged.sort_index(inplace=True)
    return merged, overlap


def fix_candle_errors(default: pd.DataFrame, filler: pd.DataFrame):
    """ Finds candles with errors (null values, infs, etc) and
    attempts to fix them using data from the secondary dataframe.
    """
    merged = default.copy()
    mask = find_candle_errors(default)['any']
    overlap = filler.loc[filler.index & mask[mask.values].index]
    merged.update(overlap)
    merged.sort_index(inplace=True)
    merged = drop_error_candles(merged)

    return merged


def drop_error_candles(df, on_all=False):
    """ Removes candles with certain types of errors
    (those listed in `drop_on_errors`).
    """
    errors = find_candle_errors(df)
    columns = drop_on_errors if not on_all else errors.columns
    errors = [errors[c] for c in columns]
    mask = functools.reduce(lambda x, y: x | y, errors)
    df = df[~mask]
    return df


def forward_fill(candles, fill_volume=False):
    """ Fills missing candles using the last close value. Optionally forward fills volume.

    Should be called after drop_error_candles. If present, `candle_number`
    column values will be set to zero.
    """
    # make sure that no nulls are introduced / all columns are explicitly handled
    for c in candles.columns:
        if c not in ('timestamp', 'open', 'high', 'low', 'close',
                     'volume', 'candle_number'):
            raise Exception(f"`candle_merge.forward_fill` can't handle column `{c}`")

    full_index = pd.date_range(candles.index[0], candles.index[-1], freq='H')
    candles = candles.reindex(full_index)

    candles.close = candles.close.ffill()
    candles.open = candles.open.fillna(candles.close)
    candles.high = candles.high.fillna(candles.close)
    candles.low = candles.low.fillna(candles.close)
    if fill_volume:
        candles.volume = candles.volume.ffill()
    else:
        candles.volume = candles.volume.fillna(0)

    if 'candle_number' in candles.columns:
        candles.candle_number = candles.candle_number.fillna(0)

    return candles


def add_start_end(primary, secondary):
    """ Adds candles before/after primary start/end, if the data
    is available in `secondary`.
    """
    start = secondary.index[secondary.index < primary.index[0]]
    end = secondary.index[secondary.index > primary.index[-1]]
    parts = [primary]
    parts += [secondary.loc[i] for i in (start, end) if len(i)]

    result = pd.concat(parts)
    result.sort_index(inplace=True)
    return result


def clean_data(primary, secondary, fill_missing=False):
    """ Runs all candle cleaning.
    """
    primary = drop_error_candles(primary)
    secondary = drop_error_candles(secondary, on_all=True)

    primary, _ = fill_gaps(primary, secondary)
    primary = add_start_end(primary, secondary)
    primary = fix_candle_errors(primary, secondary)
    if fill_missing:
        primary = forward_fill(primary)

    # remove potential duplicates
    primary = primary[~primary.index.duplicated(keep='first')]
    return primary


def candle_merge():
    if not target_table.__table__.exists(engine):
        target_table.__table__.create(engine)

    ex_pairs = ExPair.query.filter(and_(
        ExPair.active == True,
        ExPair.candle_1h,
        ExPair.exchange_id != 12  # Skip External
    ))

    for pair in ex_pairs:
        try:
            primary = read_df(default_table, pair)
            secondary = read_df(filler_table, pair)
            existing_data = read_df(target_table, pair)

            combined, to_delete = merge_dfs(pair, existing_data, primary, secondary)

            if combined is None:
                continue

            try:
                delete_rows(pair, to_delete)
                combined = combined.rename_axis('timestamp')
                combined.to_sql(target_table.__tablename__, engine,
                                index=True, if_exists='append', chunksize=10000,
                                schema='coincube')
                db_session.commit()
            except IntegrityError:
                last = (target_table.query
                        .filter(target_table.ex_pair_id == pair.id)
                        .order_by(target_table.timestamp.desc())
                        .first())
                log.error(f"Integrity error for {pair}. Last timestamp: {last}. Last existing: {existing_data.index[-1]}. \n"
                          f"To insert: {combined.index}. \n  To delete: {to_delete}")

        except Exception as e:
            log.exception(f'{pair}: Unhandled exception')


def merge_dfs(pair: ExPair, existing_data: pd.DataFrame,
              primary: pd.DataFrame, secondary: pd.DataFrame) \
        -> Tuple[pd.DataFrame, List]:
    """ Merge primary, secondary with existing data.
    """
    if primary is None and secondary is None:
        log.info(f"No data for {pair} in either table. Skipping")
        return None, None
    elif primary is None or not len(primary):
        log.info(f"No data for {pair} in {default_table.__tablename__}. "
                 f"Copying from {filler_table.__tablename__}")
        combined = secondary
    elif secondary is None or not len(secondary):
        log.info(f"No data for {pair} in {filler_table.__tablename__}. "
                 f"Copying from {default_table.__tablename__}")
        combined = primary
    else:
        combined = clean_data(primary, secondary)
        log.info(f"Merged and cleaned data for {pair}.")

    combined['ex_pair_id'] = pair.id
    return combine(combined, existing_data)


def combine(new_data: pd.DataFrame, existing_data: pd.DataFrame) -> Tuple[pd.DataFrame, List]:
    """ Combine dataframes with new and existing data.

    Two data sets are compared for differences and, if new data is different
    for existing bars, the existing bars will be deleted and new data writen
    to the table.
    """
    if new_data is None or not len(new_data):
        return existing_data, []

    if existing_data is None or not len(existing_data):
        return new_data, []

    to_delete = []
    overlap = existing_data.index & new_data.index
    overlap = overlap.sort_values()
    exo = existing_data.close[overlap]
    neo = new_data.close[overlap]
    difs = (exo - neo) / exo
    difs = difs.abs() > 0.01

    if sum(difs) > 10 or new_data.index[0] < existing_data.index[0]:
        # rebuild all
        to_delete = list(existing_data.index)
        return new_data, to_delete

    elif sum(difs) > 0:
        # find first diff
        idx = np.argmax(difs.values)
        ts = overlap[idx] - timedelta(minutes=30)
        dif_existing = existing_data.truncate(before=ts)
        to_delete = list(dif_existing.index)
        existing_data = existing_data.truncate(after=ts)

    ts = existing_data.index[-1] + timedelta(hours=1)
    new_data = new_data.truncate(before=ts)

    return new_data, to_delete


def delete_rows(ex_pair: ExPair, to_delete):
    if to_delete is None or not len(to_delete):
        return

    log.debug(f"Deleting rows {ex_pair}: {min(to_delete)} - {max(to_delete)}")
    (target_table.query
     .filter(and_(target_table.timestamp.in_(to_delete),
                  target_table.ex_pair_id == ex_pair.id))
     .delete(synchronize_session='fetch'))
    db_session.commit()


if __name__ == '__main__':
    candle_merge()
