FROM continuumio/anaconda3:5.0.1
RUN apt-get update && apt-get install -y build-essential

# Python packages
RUN conda install -c bioconda mysqlclient
RUN conda install -c conda-forge passlib flask-login flask-wtf flask-mail flask-user celery
RUN pip install flask-bcrypt

COPY requirements.txt /data/requirements.txt
RUN pip install -r /data/requirements.txt

COPY . /data
WORKDIR /data

ENTRYPOINT ["/opt/conda/bin/python"]