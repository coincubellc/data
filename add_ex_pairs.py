#!/usr/bin/env python3
from common import *
from pprint import pprint
import sys
import exapi_client as api

log = logging.getLogger(__name__)
logging.basicConfig(level=logging.DEBUG)

SYM_MAP = {
    'HitBTC': {
        'BCC': 'BCH'
    },
    'Kraken': {
        'XBT': 'BTC',
        'XDG': 'DOGE'
    },
    'Poloniex': {
        'STR': 'XLM'
    },
    'Bittrex': {
        'BCC': 'BCH'
    },
    'Bitfinex': {
        'IOT': 'MIOTA',
        'DSH': 'DASH',
        'QTM': 'QTUM'
    },
    'Binance': {
        'BCC': 'BCH',
        'IOTA': 'MIOTA'
    }
}

MIN_VOL = 5 # BTC
BTC = 'BTC'

EX_SYM_MAP = {ex_name: {} for ex_name in SYM_MAP}
for ex_name in SYM_MAP:
    for k, v in SYM_MAP[ex_name].items():
        EX_SYM_MAP[ex_name][v] = k

def get_sym(ex_name, ex_sym):
    try:
        return SYM_MAP[ex_name][ex_sym]
    except KeyError:
        return ex_sym

def get_ex_sym(ex_name, sym):
    try:
        return EX_SYM_MAP[ex_name][sym]
    except KeyError:
        return sym

btc = Currency.query.filter_by(symbol=BTC).one()
for ex in Exchange.query.filter_by(active=True).all():
    
    if ex.name in ['External', 'Manual', 'GDAX']:
        continue

    log.info(f'Updating ex_pairs for {ex.name}.')

    missing_curs = []
    summary = {}

    try:
        log.debug('Get summary (API)')
        exchange = '/' + ex.name
        summary = api.get_summary(exchange)
        log.debug(summary)
    except AttributeError:
        continue
    except Exception as e:
        log.info('%s: %s' % (ex.name, e))
        continue
    btc_ex_sym = get_ex_sym(ex.name, btc.symbol)
    if summary:
        for ex_sym, s in summary[btc_ex_sym].items():
            qvol = s['volume'] * (s['last'] or 0)
            if qvol < MIN_VOL:
                continue
            sym = get_sym(ex.name, ex_sym)
            cur = Currency.query.filter_by(symbol=sym).first()
            
            if not cur:
                missing_curs.append(ex_sym)
                continue
            ep = ExPair.query.filter_by(exchange=ex, base_symbol=ex_sym, quote_symbol=btc_ex_sym).first()
            if ep:
                # expair already exists
                continue
            epcount = ExPair.query.filter_by(base_currency=cur, quote_currency=btc).count()
            if ex_sym != sym or btc_ex_sym != btc.symbol:
                log.info(f'{ex.name} {ex_sym}/{btc_ex_sym} ({sym}/{btc.symbol}) ({epcount} existing EPs)')
            else:
                log.info(f'{ex.name} {ex_sym}/{btc_ex_sym} ({epcount} existing EPs)')

            ep = ExPair(
              exchange=ex,
              base_currency=cur,
              quote_currency=btc,
              base_symbol=ex_sym,
              quote_symbol=btc_ex_sym,
              active=True,
              candle_1h=True
              )
            db_session.add(ep)
            log.info(f'Creating ex_pair: {ep}')
        if missing_curs:
            log.debug('Missing currencies for %s: %s' % (ex.name, ' '.join(missing_curs)))

db_session.commit()
