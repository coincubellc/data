import os
from celery import Celery
from celery.schedules import crontab
from candle_collector import collect_candles_ex
from candle_collector_coinapi import collect_candles_coinapi
from candle_collector_crypto_compare import collect_candles_cc
from candle_merge import candle_merge
from market_cap_collector import run_market_cap_collector
from index_candle_generator import generate_index
from check_data import check
from database import db_session

REDIS_URI = os.getenv('REDIS_URI')

# commands:
# celery beat: celery -A celery_update beat
# celery worker(s): celery worker -A celery_update -l info -Ofair -l INFO

app = Celery('celery_collectors',
             backend=REDIS_URI,
             broker=REDIS_URI)

class SqlAlchemyTask(app.Task):
    """An abstract Celery Task that ensures that the connection the the
    database is closed on task completion"""
    abstract = True

    def after_return(self, status, retval, task_id, args, kwargs, einfo):
        db_session.close()
        db_session.remove()   


# @app.on_after_configure.connect
# def setup_periodic_tasks(sender, **kwargs):
#     sender.add_periodic_task(
#         crontab(minute=0,
#             hour=[1,3,5,7,9,11,13,15,17,21,23]),
#         update_cc_candles,
#         name='update cc candles')
#
#     sender.add_periodic_task(
#         crontab(minute=20,
#             hour=[2,4,6,8,10,12,14,16,18,20,22]),
#         merge_candles,
#         name='merge candles')
#
#     sender.add_periodic_task(
#         crontab(minute=0,
#             hour=[4, 8, 12, 16, 22]),
#         update_candles_coinapi,
#         name='update candles coinapi')
#
#     sender.add_periodic_task(
#         crontab(minute=40),
#         update_market_cap,
#         name='update market cap')
#
#     sender.add_periodic_task(
#         crontab(hour=20, minute=40),
#         check_index,
#         name='check index')
#
#     sender.add_periodic_task(
#         crontab(minute=15),
#         update_index,
#         name='update index')

@app.task(base=SqlAlchemyTask)
def update_cc_candles():
    collect_candles_cc()

@app.task(base=SqlAlchemyTask)
def update_ex_candles():
    collect_candles_ex()

@app.task(base=SqlAlchemyTask)
def update_candles_coinapi():
    collect_candles_coinapi()

@app.task(base=SqlAlchemyTask)
def merge_candles():
    candle_merge()

@app.task(base=SqlAlchemyTask)
def update_market_cap():
    run_market_cap_collector()

@app.task(base=SqlAlchemyTask)
def check_index():
    check()

@app.task(base=SqlAlchemyTask)
def update_index():
    generate_index()
