import json
import logging
from database import Currency, db_session, Exchange, ExPair

log = logging.getLogger(__name__)
logging.basicConfig(level=logging.DEBUG)

with open('erc20tokens.json', 'r') as f:
    tokens = json.load(f)

# ERC20 list
erc20_list = []
for token in tokens:
    if ' ' in token['symbol']:
        symbol = token['symbol'].split(' ')[0]
    else:
        symbol = token['symbol']
    erc20_list.append(symbol)

# Compare erc20 list to support currencies
curs = Currency.query.all()
for cur in curs:
    if cur.symbol in erc20_list:
        cur.base_protocol = 'ethereum'
        db_session.add(cur)

db_session.commit()

# Update External ExPairs with all ERC20 tokens
erc20_curs = Currency.query.filter_by(base_protocol='ethereum').all()
quote_cur = Currency.query.filter_by(symbol='BTC').first()
exchange = Exchange.query.filter_by(id=12).first()

log.info('Updating missing External ERC20 ExPairs')
for cur in erc20_curs:

    ep = ExPair.query.filter_by(exchange=exchange, base_currency=cur, quote_currency=quote_cur).first()
    if ep:
        log.info(f'{ep} Already exists... skipping')
        # expair already exists
        continue

    ep = ExPair(
      exchange=exchange,
      base_currency=cur,
      quote_currency=quote_cur,
      base_symbol=cur.symbol,
      quote_symbol=quote_cur.symbol,
      active=True,
      candle_1h=True
      )
    db_session.add(ep)
    log.info(f'Creating ex_pair: {ep}')

db_session.commit()