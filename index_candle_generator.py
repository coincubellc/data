#!/usr/bin/env python3

import logging
from time import sleep

import pandas as pd
from sqlalchemy import and_

from candle_merge import forward_fill, drop_error_candles, combine
from common import read_df
from database import (Currency, ExPair, IndexPair, UniqueConstraint, relationship, Column,
    FKInteger, ForeignKey, CandleMixin, Numeric, CombinedExPair1h, Integer, CombinedIndexPair1h,
    engine, db_session, Base)

MIN_SOURCES = 1  # required number of candles from different exchanges

N_DECIMALS = 12  # number of decimal places OHLCV values should be rounded to
RUN_INTERVAL = 3600  # seconds (1 hour)
FFILL_VOLUME = False  # whether to forward fill volume or to set to zero

# should the nonexistent candles be created (and forward filled)
FFILL_MISSING_CANDLES = True

log = logging.getLogger(__name__)

source_table = CombinedExPair1h
target_table = CombinedIndexPair1h


def create_index_candles(ex_pairs, ip, current):
    candles = pd.DataFrame(columns=['close', 'open', 'high', 'low', 'volume'])

    for ep in ex_pairs:

        epc = read_df(source_table, ep)
        candles = candles.append(epc)

    candles['candle_number'] = 1

    # vwap
    candles.close *= candles.volume
    candles.open *= candles.volume
    candles.high *= candles.volume
    candles.low *= candles.volume
    candles = candles.groupby(candles.index).sum()
    candles.close /= candles.volume
    candles.open /= candles.volume
    candles.high /= candles.volume
    candles.low /= candles.volume

    candles = drop_error_candles(candles)
    if not len(candles):
        return candles, []
    if FFILL_MISSING_CANDLES:
        candles = forward_fill(candles, FFILL_VOLUME)

    base_id = ex_pairs[0].base_currency_id
    quote_id = ex_pairs[0].quote_currency_id
    candles = candles.round(N_DECIMALS)
    candles.index.name = 'timestamp'
    candles['base_currency_id'] = base_id
    candles['quote_currency_id'] = quote_id
    candles['index_pair_id'] = ip.id

    # only use candles if they're built with `MIN_SOURCES`
    # or more exchange candles
    if MIN_SOURCES:
        candles = candles[candles.candle_number >= MIN_SOURCES]

    # select a subset of new/updated candles
    if current is not None and len(current):
        new_candles = get_new_candles(candles, current)
        # find candles with updated data
        new_data, _ = combine(candles, current)
        new_idx = new_candles.index | new_data.index
        new_candles = candles.loc[new_idx]
        to_delete = new_candles.index & current.index
    else:
        new_candles = candles
        to_delete = []

    return new_candles, to_delete


def get_new_candles(candles, current):
    """ Selects the relevant subset of candles (new candles or updated old ones)
    """
    # avoid pandas 'set on a copy of a slice' warning
    current = current['candle_number'].copy()

    # find candles with more sources than existing in the table
    common_idx = candles.index.intersection(current.index)
    sel = candles.loc[common_idx]['candle_number'].gt(current.loc[common_idx])
    sel = sel[sel.values == True].index

    # assure we add candles that aren't in the table
    new_idx = candles.index.difference(common_idx)
    idx = new_idx.join(sel, 'outer')
    candles = candles.loc[idx].sort_index()
    return candles


def update_index_pairs():
    """ Creates index pairs (if necessary) for all individual ExPairs.
    """
    log.debug("Updating IndexPair table")
    # Query for all Currencies
    all_curs = Currency.query.all()
    # Quote currencies: BTC, USD, EUR
    quote_curs = Currency.query.filter(Currency.symbol.in_(['BTC', 'USD', 'EUR', 'USDT', 'TUSD', 'USDC'])).all()

    for quote_cur in quote_curs:
        for base_cur in all_curs:
            # Check for any ExPair for given base/quote
            ex_pair = ExPair.query.filter_by(
                quote_currency_id=quote_cur.id,
                base_currency_id=base_cur.id,
                active=True).first()
            if not ex_pair:
                continue

            # ExPair exists, so we need a matching IndexPair
            index_pair = IndexPair.query.filter_by(
                quote_currency_id=quote_cur.id,
                base_currency_id=base_cur.id).first()

            if index_pair:  # IndexPair already exists
                continue
            else:
                log.debug("Adding missing IndexPair for %s/%s" %
                          (base_cur.symbol, quote_cur.symbol))
                # Missing IndexPair, create new one
                ip = IndexPair(
                    quote_currency_id=quote_cur.id,
                    base_currency_id=base_cur.id,
                    quote_symbol=quote_cur.symbol,
                    base_symbol=base_cur.symbol,
                    active=True,
                    candle_1h=True
                )
                db_session.add(ip)
                db_session.commit()


def generate_index():
    """ Builds Index Pairs and the updates index data for all pairs.
    """
    if not target_table.__table__.exists(engine):
        target_table.__table__.create(engine)

    update_index_pairs()
    build_all_pairs()


def build_all_pairs():
    # Grab all active IndexPairs
    index_pairs = IndexPair.query.filter_by(active=True).all()

    for ip in index_pairs:
        log.info(f"Updating pair {ip}")
        update_pair(ip)

    log.info("Done.")


def delete_rows(ip: IndexPair, to_delete):
    """ Deletes rows for the IndexPair where timestamps are in `to_delete`.
    """
    if to_delete is None or not len(to_delete):
        return

    log.debug(f"Deleting rows {ip}: {to_delete.min()} - {to_delete.max()}")
    (target_table.query
     .filter(and_(target_table.timestamp.in_(to_delete),
                  target_table.index_pair_id == ip.id))
     .delete(synchronize_session='fetch'))
    db_session.commit()


def update_pair(ip):
    """ Builds/updates candles for an individual IndexPair.
    """
    # Get all related, active and non-External ExPairs
    ex_pairs = ExPair.query.filter(
        ExPair.quote_currency_id == ip.quote_currency_id,
        ExPair.base_currency_id == ip.base_currency_id,
        ExPair.active == True,
        ExPair.exchange_id != 12,  # External
        ExPair.exchange_id != 13
    ).all()

    if not ex_pairs:
        log.info(f"No exchange pairs available for {ip}.")
        return

    db_data = read_df(target_table, ip)
    try:
        # Create vwap index candles
        candles, to_delete = create_index_candles(ex_pairs, ip, db_data)
        log.debug(f"To insert: {list(candles.index)}")
        log.debug(f"To delete: {list(to_delete)}")
    except Exception as e:
        log.warn(f"Unable to update candles because of error")
        log.warn(e)        
        return

    # Commit to database
    if not candles.empty:
        try:
            timestamp = None if db_data is None else db_data.index[-1].timestamp
            delete_rows(ip, to_delete)
            log.debug(f"Updating {ip} from {timestamp or 'start'}.")

            candles.to_sql(target_table.__tablename__, engine,
                           index=True, if_exists='append', 
                           chunksize=10000, schema='coincube')
            db_session.commit()
        except Exception as e:
            log.warn(f"Unable to update candles because of error")
            log.warn(e)
    else:
        log.info("No new/updated candles.")


if __name__ == '__main__':
    logging.basicConfig(level=logging.DEBUG)
    while True:
        try:
            generate_index()
        except Exception as e:
            log.exception('Unhandled exception')
        finally:
            db_session.close()
            sleep(RUN_INTERVAL)
