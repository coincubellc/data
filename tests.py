import datetime
import unittest

import numpy as np
import pandas as pd

from candle_merge import fill_gaps, forward_fill, add_start_end, fix_candle_errors, drop_error_candles, combine, merge_dfs
from index_candle_generator import get_new_candles


def _make_ohclv_df(start=None, end=None) -> pd.DataFrame:
    if not start:
        start = datetime.datetime(2018, 1, 1)
    if not end:
        end = datetime.datetime(2018, 1, 2)
    rng = pd.date_range(start, end=end, freq='H')
    size = len(rng)

    # build a mock dataframe that's easy to debug
    build = lambda x: np.arange(x, x + (x / 100 * size), x / 100)
    data = dict(
        open=build(2),
        high=build(3),
        low=build(1),
        close=build(4),
        volume=build(1000),
    )

    df = pd.DataFrame(data, rng)
    return df



class TestMerge(unittest.TestCase):
    def test_fill_gaps(self):
        start = datetime.datetime(2018, 1, 1)
        end = datetime.datetime(2018, 1, 2)
        rng = pd.date_range(start, end=end, freq='H')
        orig = pd.DataFrame({'c': np.ones(len(rng))}, index=rng)
        p = orig.copy()
        s = p.copy()

        # create gaps
        gap = p.index[3:5]
        p = p.drop(gap)

        res, diff = fill_gaps(p, s)
        self.assertTrue(orig.equals(res))
        self.assertTrue(gap.equals(diff.index))

    def test_forward_fill(self):
        p = _make_ohclv_df()

        # create gap
        gap = p.index[3:5]
        p = p.drop(gap)

        res = forward_fill(p, fill_volume=True)
        data = {c: p.iloc[2]['close']
                for c in ['open', 'high', 'low', 'close']}
        data['volume'] = p.iloc[2]['volume']

        expected_fill = pd.DataFrame(data, gap)
        self.assertTrue(res.loc[gap].equals(expected_fill))

    def test_add_start_end(self):
        orig = _make_ohclv_df()
        p = orig.copy()
        s = p.copy()

        gap = p.index[0:2] | p.index[-1:]
        p = p.drop(gap)

        res = add_start_end(p, s)
        self.assertTrue(res.equals(orig))

    def test_fix_candle_errors(self):
        orig = _make_ohclv_df()
        p = orig.copy()
        s = p.copy()

        # add some errors
        p.iloc[0, 1] = np.nan
        p.iloc[1, 2] = np.inf
        p.iloc[2, 3] = 0
        p.loc[p.index[3], 'high'] = p.loc[p.index[3], 'low']
        p.loc[p.index[5], 'low'] = p.loc[p.index[5], 'high']

        res = fix_candle_errors(p, s)
        self.assertTrue(res.equals(orig))

    def test_drop_error_candles(self):
        p = _make_ohclv_df()

        # add some errors
        p.iloc[0, 1] = np.nan

        p.iloc[1:3, 3] = 0

        res = drop_error_candles(p)
        self.assertTrue(res.index.equals(p.index[3:]))

    def test_combine(self):
        # no old, new
        df = _make_ohclv_df()
        res = combine(df, pd.DataFrame())[0]
        self.assertTrue(df.equals(res))
        res = combine(pd.DataFrame(), df)[0]
        self.assertTrue(df.equals(res))

        # 1-3 new bars
        old = df.iloc[:4]
        new = df.iloc[1:]
        res, to_del = combine(new, old)
        self.assertEqual(len(to_del), 0)
        self.assertTrue(res.equals(df.iloc[4:]))

        # old earlier start & some changed bars
        old = df.iloc[:4]
        new = df.iloc[1:].copy()
        new.loc[:, 'close'] = 1.0
        res, to_del = combine(new, old)
        self.assertEqual(to_del, list(old.index[1:]))
        expected = pd.concat([old.iloc[0:1], new]).iloc[1:]
        self.assertTrue(res.equals(expected))

        # 0 diffs
        old = df.iloc[:10]
        new = df.copy()
        res, to_del = combine(new, old)
        self.assertFalse(len(to_del))
        self.assertTrue(res.equals(df.iloc[10:]))

        # new earlier start
        old = df.iloc[5:10]
        new = df.copy()
        res, to_del = combine(new, old)
        self.assertEqual(to_del, list(old.index))
        self.assertTrue(res.equals(df))


class TestIndexCalc(unittest.TestCase):
    def test_get_new_candles(self):
        current = _make_ohclv_df()
        new = current.copy()

        # add new values
        data = {c: [7, 8] for c in ['open', 'high', 'low', 'close', 'volume']}
        idx = pd.date_range(datetime.datetime(2018, 1, 2, 1),
                            end=datetime.datetime(2018, 1, 2, 2), freq='H')
        n = pd.DataFrame(data, idx)
        new = new.append(n)

        # update old values
        new.iloc[1] = np.ones(5)
        new.iloc[2] = np.zeros(5)

        current['candle_number'] = 1
        new['candle_number'] = 1
        new.ix[1:3, 'candle_number'] = 2

        res = get_new_candles(new, current)
        expected = new.loc[new.index[1:3] | idx]
        self.assertTrue(res.equals(expected))
