<h1>data</h1>
This repo contains the Coincube Market Data Module.


<h2>Getting Started</h2>
In order to get up and running with this repo:

<h2>Submodules</h2>
This repo contains two submodules which need to be pulled down before you will be able to work.<br>

<h3>In order to pull in the submodule `database` you will need to run:</h3>
`git submodule init`<br>
`git submodule update`<br>

<h3>Please be sure to push submodule changes to remote</h3>
<h5 href="https://git-scm.com/book/en/v2/Git-Tools-Submodules">Git Submodule Tutorial #1</h5>
<h5 href="https://git.wiki.kernel.org/index.php/GitSubmoduleTutorial">Git Submodule Tutorial #2</h5>

<h2>Docker Setup</h2>
You will need <a href="https://docker.com" target="_blank">Docker</a>.

Build the Docker container(s):
For local environment: `docker-compose build`<br>
For production environment: `docker-compose -f docker-compose.yml -f docker-compose.prod.yml build`<br>

Run the Docker container(s):
For local environment: `docker-compose up`<br>
For production environment: `docker-compose -f docker-compose.yml -f docker-compose.prod.yml up -d`

<h3>Shell into a specific container</h3>
Find "CONTAINER ID": `docker ps`<br>

Shell into container: `docker exec -it "CONTAINER ID" bash`<br>
(i.e. `docker exec -it 78e539ca25be bash`)

<h3>View container logs</h3>
`docker logs -f "CONTAINER ID"`

<h6>Launch candle_collector</h6>
From command line: `./candle_collector.py`

<h2>Build and push images</h2>
- run docker login `$(aws ecr get-login --no-include-email)`
- build the images with the name of the repo e.g `docker build -t 648212798771.dkr.ecr.us-east-1.amazonaws.com/data:latest .`
- push the images `docker push 648212798771.dkr.ecr.us-east-1.amazonaws.com/data:latest`
Let me know if you need help with something (edited)

<h2>Data recalculation procedure</h2>
In a scenario where the data retroactively changes in a local table (`ex_pair_1h_local` or `ex_pair_1h_ccompare`),
the derived tables (`ex_pair_1h_final` and `index_pair_1h`) need to be rebuilt with the new data.
The procedure for this is as follows:
- Stop the celery tasks (`update_candles` in `celery_update_cc.py`) that update the local (source) tables
- Stop the tasks that update derived tables (`merge_candles` and `update_index` in `celery_update_cc.py`)
- Rename / wipe the local table that needs to be refreshed. Only one table, not both!
- Rename / wipe the derived tables.
- Reenable the `update_candles` task. It will most likely be required to wait for a couple of hours until
all historical data is downloaded.
- After that is done, reenable the `merge_candles` and `update_index` tasks.
