#!/usr/bin/env python3
import logging
import time
from datetime import timedelta, datetime
from time import sleep
import pandas as pd
import requests as rq
from sqlalchemy import and_
from sqlalchemy.exc import IntegrityError

from common import read_df
from database import ExPair, Candles1hCCompare, engine, db_session

RUN_INTERVAL = 3600  # seconds (1 hour)
CC_BAR_LIMIT = 2000
SLEEP_INTERVAL = 0.2

log = logging.getLogger(__name__)


# Crypto Compare URL
_base_url = 'https://min-api.cryptocompare.com/data/histohour'

table_class = Candles1hCCompare  # CombinedExPair1h


def from_crypto_compare(ex_pair, stop_ts):
    base = ex_pair.base_currency.symbol
    quote = ex_pair.quote_currency.symbol

    # Fix for discrepency between CC db and Crypto Compare
    if ex_pair.quote_symbol == 'USDT':
        quote = 'USDT'
    if base == 'IOTA':
        base = 'IOT'
    if ex_pair.exchange.name == 'CoinbasePro':
        exchange = 'Coinbase'
    else:
        exchange = ex_pair.exchange.name
    params = {
        'fsym': base,
        'tsym': quote,
        'limit': CC_BAR_LIMIT,
        'e': exchange,
    }

    try:
        req_ts = None
        dfs = []
        while True:
            # update the timestamp for the CC request
            if req_ts is not None:
                params['toTs'] = req_ts

            data = rq.get(_base_url, params=params).json()["Data"]
            if data:
                df = pd.DataFrame(data)
                if req_ts is not None:
                    df = df[df['time'] < req_ts]

                req_ts = df['time'].iloc[0]
                df['timestamp'] = pd.to_datetime(df.time, unit='s')

                # check if finished
                if df.close.sum() <= 0:
                    break
                else:
                    dfs.append(df)
                    if stop_ts is not None and df.timestamp.min() < stop_ts:
                        break

                    time.sleep(SLEEP_INTERVAL)
            else:  # no data
                if len(dfs):
                    break

                log.debug("Unable to retrieve data from Crypto Compare for %s" % ex_pair)
                return pd.DataFrame()

        df = pd.concat(dfs)
        df.set_index('timestamp', inplace=True)
        df = df.sort_index()
        df.rename(columns={'volumefrom': 'volume'}, inplace=True)

        del df['time']
        del df['volumeto']

        # clean data, since cryptocompare api returns zeros for when no data is available
        start_idx = None
        sums = df.sum(axis=1).values
        for i, v in enumerate(sums):
            if v > 0:
                start_idx = i
                break

        if start_idx is None:
            log.error(f"No historical data returned for ExPair {ex_pair}.")
            return pd.DataFrame()

        df = df.iloc[start_idx:]

        if stop_ts is not None:
            df = df.truncate(before=stop_ts).iloc[1:]

        # remove partial bar
        last_ts = datetime.utcnow()
        last_ts = last_ts.replace(minute=0, second=0, microsecond=0)
        last_ts = last_ts - timedelta(seconds=3600)
        df = df.truncate(after=last_ts)

        return df

    except Exception as e:
        log.debug(f"Unable to retrieve data from Crypto Compare for {ex_pair}. Exception: {e}" )
        return pd.DataFrame()


def collect_candles_cc():
    if not table_class.__table__.exists(engine):
        table_class.__table__.create(engine)

    ex_pairs = ExPair.query.filter(and_(
        ExPair.active == True,
        ExPair.candle_1h,
        ExPair.exchange_id != 12,  # Skip External
        ExPair.exchange_id != 13, # Skip Manual
    )).all()
    log.debug(f"{len(ex_pairs)} pairs to update.")

    for ex_pair in ex_pairs:
        log.debug(f"{ex_pair} running")
        data = read_df(table_class, ex_pair)
        join_time = data.index[-1] if data is not None else None
        if join_time:
            join_time = join_time + timedelta(hours=1)
        new_frame = from_crypto_compare(ex_pair, join_time)
        if new_frame.empty:
            log.debug("Unable to add candles for %s" % ex_pair)
            continue
        log.debug("Updating %s from %s" % (ex_pair, join_time))
        new_frame = new_frame.truncate(before=join_time)
        new_frame = new_frame[~new_frame.index.duplicated(keep='last')]
        new_frame['ex_pair_id'] = ex_pair.id
        try:
            new_frame.to_sql(table_class.__tablename__, engine,
                             index=True, if_exists='append', chunksize=10000,
                             schema='coincube')
            db_session.commit()
            log.debug("Candles updated for %s" % ex_pair)
        except IntegrityError:
            log.debug("Unable to add candles for %s" % ex_pair)

    log.info("Done.")


if __name__ == '__main__':
    logging.basicConfig(level=logging.DEBUG)
    while True:
        try:
            collect_candles_cc()
        except Exception as e:
            log.exception('Unhandled exception')
        finally:
            sleep(RUN_INTERVAL)
