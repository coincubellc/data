from slacker import Slacker

from common import set_logger
from database import *


###############################################################

SLACK_API_TOKEN = 'xoxp-3282693918-3282693920-143564691477-7ec7c2e407bbb17423943fea24114b73'
SLACK_CHANNEL = 'data'

DEBUG = False

# if the delay from the last timestamp to now is more
# than this number of hours, show an error
MAX_UPDATE_DELAY = 4
REMOVE_THRESHOLD = 10

fiats = ['USD', 'EUR', 'GBP', 'CAD']

skip_external = True

# list of pair ids to skip
currencies_skip = []
index_pairs_skip = []
ex_pairs_skip = []

################################################################


errs = []
slack = Slacker(SLACK_API_TOKEN)

log = set_logger(name=__name__, sysloglevel=None, emaillevel=None)

def post_message(msg):
    log.info(msg)
    try:
        slack.chat.post_message(SLACK_CHANNEL, msg)
    except:
        log.exception("Slack exception:")

def check():
    try:
        post_message("Starting data check.")
        check_index_pairs()
        check_ex_pairs()
        post_message("Data check finished.")
    except:
        log.exception("Exception while performing data check:")

def deactivate(ip):
    # Remove ExPairs
    ex_pairs = ExPair.query.filter_by(base_symbol=ip.base_symbol).all()
    for ex_pair in ex_pairs:
        ex_pair.active = False
        db_session.add(ex_pair)
    # Remove IndexPair
    ip.active = False
    db_session.add(ip)
    db_session.commit()

def check_ex_pairs():
    post_message("Checking ExPairs.")
    ex_pairs = ExPair.query.filter_by(active=True).all()
    ex_pair_errors = 0

    for ep in ex_pairs:
        if skip_external and ep.exchange.name == 'External':
            continue

        if not ep.active:
            continue

        if ep.id in ex_pairs_skip:
            continue

        if DEBUG:
            print(ep)

        try:
            ts = (CombinedExPair1h.query
                  .filter(CombinedExPair1h.ex_pair_id == ep.id)
                  .order_by(CombinedExPair1h.timestamp.desc())
                  .first()).timestamp
        except:
            s = f"Error at {ep}: Could not load data from the database. "
            post_message(s)
            ex_pair_errors += 1
            continue

        now = datetime.utcnow()
        dif = int((now - ts).total_seconds() / 3600)

        if DEBUG:
            print(dif, ts)
        if dif > MAX_UPDATE_DELAY:
            s = f"Error: Ex pair {ep} hasn't been updated in {dif} hours. Last timestamp: {ts}"
            post_message(s)
            ex_pair_errors += 1

        index_pairs = IndexPair.query.filter_by(base_currency_id=ep.base_currency_id).all()
        if not len(index_pairs):
            post_message(f"Error: Ex pair {ep} has no matching index pairs.")

    post_message(f"{ex_pair_errors}/{len(ex_pairs)} ex pairs with errors.")

def check_index_pairs():
    post_message("Checking IndexPairs.")
    index_pairs = IndexPair.query.filter_by(active=True).all()
    index_pair_errors = 0
    for ip in index_pairs:
        if ip in index_pairs_skip:
            continue

        if not ip.active:
            continue

        if DEBUG:
            print(ip)
        try:
            ts = (CombinedIndexPair1h.query
                  .filter(CombinedIndexPair1h.index_pair_id == ip.id)
                  .order_by(CombinedIndexPair1h.timestamp.desc())
                  .first()).timestamp
        except:
            s = f"Error @ {ip}: Could not load data from the database. "
            post_message(s)
            index_pair_errors += 1
            # deactivate(ip)
            continue

        now = datetime.utcnow()
        dif = int((now - ts).total_seconds() / 3600)

        if dif > MAX_UPDATE_DELAY:
            s = f"Error: {ip} hasn't been updated in {dif} hours. Last timestamp: {ts}"
            post_message(s)
            index_pair_errors += 1
            # if dif > REMOVE_THRESHOLD:
            #     deactivate(ip)

    post_message(f"{index_pair_errors}/{len(index_pairs)} index pairs with errors.")


if __name__ == '__main__':
    check()
