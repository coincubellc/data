#!/usr/bin/env python3
import bs4
import numpy as np
import pandas as pd

from common import *


# MIN_MARKET_CAP = 1000000
RUN_INTERVAL = 60*60 * 24


log = logging.getLogger(__name__)


# limit requests to no more than 30 per minute
REQUEST_INTERVAL = 3

DATA_START = '20130101'

_base_url = 'https://pro-api.coinmarketcap.com'
headers = {'X-CMC_PRO_API_KEY': '932e9896-d43d-4f5e-87e1-ac4d47ba8df6'}

TICKER_URL = 'https://api.coinmarketcap.com/v2/ticker/?start={}&limit=100&sort=id'
DATA_URL = 'https://coinmarketcap.com/currencies/{}/historical-data/?start={}&end={}'
MARKETS_URL = 'https://api.coinmarketcap.com/v2/ticker'

def get_markets():
    # Get market data from CMC
    log.info("Getting markets from CoinMarketCap...")
    url = _base_url + '/v1/cryptocurrency/listings/latest?start=1&limit=3000'
    resp = requests.get(url, headers=headers)
    if resp.status_code == 200:
        markets = resp.json()['data']
        return markets
    else:
        return []

def update_index_assets(markets):
    # Rank dictionary
    ranks = {}
    usd_removed = 0
    for market in markets:
        symbol = market['symbol']
        if symbol == 'MIOTA':
            symbol = 'IOTA'        
        # Remove USDT from index
        if symbol in ['USDT', 'TUSD', 'USDC', 'USDS']:
            usd_removed += 1
            continue
        if not usd_removed:
            ranks[market['cmc_rank']] = symbol
        else:
            ranks[market['cmc_rank'] - usd_removed] = symbol

    # Update index assets by rank
    indices = Focus.query.all()
    for index in indices:
        index_count = index.count
        log.info(f"Updating {index.type} index assets by rank...")
        index_curs = []
        for rank in ranks.keys():
            if rank <= index_count:
                cur = Currency.query.filter_by(symbol=ranks[rank]).first()
                if cur:
                    index_curs.append(cur)
        log.info(index.type)
        log.info(index_curs)
        index.currencies = index_curs
        db_session.add(index)
    db_session.commit()
    log.info("Updated all indices by rank.")

def update_currencies(markets):
    log.info("Updating all currencies...")
    # Loop through markets and update currencies
    for market in markets:
        symbol = market['symbol']
        if symbol == 'MIOTA':
            symbol = 'IOTA'
        cur = Currency.query.filter_by(symbol=symbol).first()
        log.info(f"Updating {cur}")
        log.info(market)
        if cur:
            cur.num_market_pairs = market['num_market_pairs']
            cur.circulating_supply = market['circulating_supply']
            cur.total_supply = market['total_supply']
            cur.max_supply = market['max_supply']
            cur.price = market['quote']['USD']['price']
            cur.volume_24h = market['quote']['USD']['volume_24h']
            if market['quote']['USD']['percent_change_1h']:
                cur.percent_change_1h = float(market['quote']['USD']['percent_change_1h'])
            else: cur.percent_change_1h = 0
            if market['quote']['USD']['percent_change_24h']:
                cur.percent_change_24h = float(market['quote']['USD']['percent_change_24h'])
            else:
                cur.percent_change_24h = 0
            if market['quote']['USD']['percent_change_7d']:
                cur.percent_change_7d = float(market['quote']['USD']['percent_change_7d'])
            else:
                cur.percent_change_7d = 0
            if market['quote']['USD']['market_cap']:
                cur.market_cap = market['quote']['USD']['market_cap']
            else:
                cur.market_cap = 0
            cur.save_to_db()
        else:
            pass
    db_session.commit()


def update_cmc_table(markets):
    log.info("Updating CMC table...")
    currencies = Currency.query.filter(Currency.market_cap > 0).all()
    timestamp = datetime.utcnow()
    timestamp = timestamp.replace(hour=0, minute=0, second=0, microsecond=0)
    for cur in currencies:
        log.debug(f'Updating {cur}')
        cmc = CoinMarketCap(
                    symbol=cur.symbol,
                    market_cap=cur.market_cap,
                    price=cur.price,
                    volume=cur.volume_24h,
                    timestamp=timestamp
                    )
        db_session.add(cmc)
    db_session.commit()
    log.info("Finished updating CMC table.")



def get_tickers():
    """ Get a list of ticker data from CoinMarketCap,
    so that website slugs can be used.
    """
    current_id = 1
    results = {}

    log.info("Getting tickers from CoinMarketCap...")

    for _ in range(50):
        try:
            url = _base_url + '/v1/cryptocurrency/listings/latest/?start={current_id}'
            resp = requests.get(url, headers=headers)
            data = resp.json()['data']

            if data is None:  # reached max id
                break

            log.debug("Received {} tickers.".format(len(data)))

            results.update(data)

            if len(data) < 100:  # reached max id
                break

            current_id += 100
            sleep(REQUEST_INTERVAL)
        except Exception as e:
            log.exception("Unhandled exception in market_cap_collector.get_tickers: ")

    log.debug("Finished downloading tickers from CoinMarketCap. {} total.".format(len(results)))
    results = {t['symbol']: t for t in results.values()}

    return results


# taken from:
# github.com/dkamm/coinmarketcap-scraper
def parse_historical_coin_response(resp) -> pd.DataFrame:
    soup = bs4.BeautifulSoup(resp.text, 'lxml')
    soup_hist = soup.find(id='historical-data')
    if not soup_hist:
        return

    table = soup_hist.find('table')
    # they added *'s to the end of some columns
    columns = [x.text.lower().replace(' ', '').rstrip('*')
               for x in table.thead.find_all('th')]

    def get_val(td):
        # numeric columns store their value in this attribute in addition to text
        val = td.get('data-format-value')
        if val:
            try:
                return np.float64(val)
            except ValueError:
                return np.nan
        return td.text

    rows = []
    for tr in table.tbody.find_all('tr'):
        if tr.td.text == 'No data was found for the selected time period.':
            return
        rows.append([get_val(x) for x in tr.find_all('td')])

    df = pd.DataFrame(columns=columns, data=rows)
    df['date'] = pd.to_datetime(df.date)
    df = df.set_index('date')

    return df


def datetime_to_str(dt):
    return dt.isoformat().split('T')[0].replace('-', '')


def get_symbol_mcap(tickers, symbol: str) -> pd.DataFrame:
    """ Get CMC data for an individual symbol.
    """

    log.info("Downloading CMC data for {}".format(symbol))
    last_ts = CoinMarketCap.query.filter_by(
        symbol=symbol).order_by(
        CoinMarketCap.timestamp.desc()).first()

    if last_ts is None:
        last_ts = DATA_START
    else:
        last_ts = str(last_ts.timestamp).split()[0].replace('-', '')

    today = datetime_to_str(datetime.utcnow())
    if today == last_ts:
        log.info("Symbol CMC data up to date. Skipping.")
        return

    log.debug("Getting the data for period {} - {}".format(last_ts, today))

    cmc_symbol = tickers[symbol]['website_slug']
    url = DATA_URL.format(cmc_symbol, last_ts, today)
    resp = requests.get(url)
    df = parse_historical_coin_response(resp)

    if df is None:
        log.error("Received None from 'parse_historical_coin_response'")
        return

    start_dt = df.index[0]
    if datetime_to_str(start_dt) == last_ts:
        df = df.iloc[1:]

    df = (df.drop(['open', 'high', 'low'], axis=1)
          .reset_index()
          .rename(columns={'marketcap': 'market_cap',
                           'close': 'price',
                           'date': 'timestamp'})
          .assign(symbol=symbol))

    # the values returned sometimes aren't rounded
    df['timestamp'].hour = 0
    df['timestamp'].minute = 0
    df['timestamp'].second = 0

    return df


def collect_market_cap():
    """ Collect CMC data for all symbols in the 'currencies' table.
    """
    log.info("Collecting data from CoinMarketCap.")

    if not CoinMarketCap.__table__.exists(engine):
        CoinMarketCap.__table__.create(engine)

    tickers = get_tickers()

    currencies = Currency.query.all()
    for cur in currencies:
        if cur.symbol not in tickers:
            log.error(f'Symbol {cur.symbol} not on CoinMarketCap.')
            continue

        df = None
        try:
            df = get_symbol_mcap(tickers, cur.symbol)
        except:
            log.exception("Exception while downloading historical data for symbol {}:".format(cur.symbol))

        if df is None or not len(df):
            continue

        # Update currency.market_cap
        print(f'Updating {cur.symbol} market cap to {df["market_cap"].iloc[-1]}')
        cur.market_cap = df["market_cap"].iloc[-1]
        cur.save_to_db()

        # write to db
        df.to_sql(CoinMarketCap.__tablename__, engine,
                  index=False, if_exists='append', chunksize=10000,
                  schema='coincube')
        db_session.commit()

        sleep(REQUEST_INTERVAL)

    log.info("Finished collecting CMC data for all symbols.")


def run_market_cap_collector():
    try:
        markets = get_markets()
        update_currencies(markets)
    except Exception as e:
        log.exception('Unhandled exception', e)
    try:
        update_index_assets(markets)
    except Exception as e:
        log.exception('Unhandled exception', e)
    try:
        update_cmc_table(markets)
    except Exception as e:
        log.exception('Unhandled exception', e)
    finally:
        db_session.close()

if __name__ == '__main__':
    # logging.basicConfig(level=logging.DEBUG)
    log.setLevel(logging.DEBUG)
    ch = logging.StreamHandler()
    ch.setLevel(logging.DEBUG)
    cf = logging.Formatter('[%(asctime)s] %(levelname)s: %(message)s',
                           datefmt='%H:%M:%S')
    ch.setFormatter(cf)
    log.addHandler(ch)

    while True:
        try:
            markets = get_markets()
            update_index_assets(markets)
            update_currencies(markets)
            update_cmc_table(markets)
            # collect_market_cap()
        except Exception as e:
            log.exception('Unhandled exception')
        finally:
            db_session.close()
            sleep(RUN_INTERVAL)