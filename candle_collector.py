#!/usr/bin/env python3
import pytz

from common import *
import exapi_client as api

RUN_INTERVAL = 60

table_class = Candles1hLocal  # CombinedExPair1h

log = set_logger(name=__name__, sysloglevel=None, emaillevel=None)


def get_last(ep: ExPair):
    """ Returns the last row for the ExPair, or None if nonexistent.
    """
    last = (table_class.query
            .filter(table_class.ex_pair_id == ep.id)
            .order_by(table_class.timestamp.desc())
            .first())

    return last


def collect_candles_ex():
    if not table_class.__table__.exists(engine):
        table_class.__table__.create(engine)

    ex_pairs = ExPair.query.filter(
        or_(
            ExPair.exchange.has(name='Poloniex'),
            ExPair.exchange.has(name='Bittrex'),
            ExPair.exchange.has(name='Binance'),
            ExPair.exchange.has(name='CoinbasePro'),
        ),
    ).order_by(ExPair.id).all()

    h_delta = pd.Timedelta('1h')
    log.info(f'{len(ex_pairs)} symbols to download.')
    for ep in ex_pairs:
        if not ep.active:
            # Skip inactive ex_pairs
            continue

        try:
            last = get_last(ep)
            if last:
                start = last.timestamp + h_delta
                close = last.close
            else:
                start = datetime.utcnow() - pd.Timedelta(days=30)

                close = None
            if last is not None and datetime.utcnow() - last.timestamp < h_delta * 2:
                log.info(f"{ep} up to date: {last.timestamp}")
            else:
                log.info(f"Updating pair: {ep}. Last timestamp: {None if last is None else last.timestamp}")

            start = start.replace(tzinfo=pytz.UTC)
            retries = 0
            while datetime.utcnow().replace(tzinfo=pytz.UTC) - start > h_delta:
                # avoid getting stuck in an infinite loop if the exchange is down
                # and we keep getting ConnectionError
                if retries > 20:
                    break

                if ep.exchange.name == 'CoinbasePro':
                    limit = None
                else:
                    limit = 1000

                if ep.exchange.name == 'Poloniex':
                    interval = '2h'
                else:
                    interval = '1h'

                h = api.get_candles(exchange_name=ep.exchange.name, 
                                    base=ep.base_currency.symbol, 
                                    quote=ep.quote_currency.symbol,
                                    interval=interval,
                                    since=int(start.timestamp() * 1000),
                                    limit=limit,
                                    )

                if h is None or h is False or not len(h):
                    log.info("No data recieved.")
                    break

                h = pd.read_json(h)
                h = h.rename_axis('timestamp')
                h = h.sort_index()
                if not len(h):
                    break
                    
                # GDAX returns no data for non-trading periods.
                # Must fill using last candle.
                if h.close.iloc[0] != h.close.iloc[0]:  # nan
                    if close:
                        h.close.iloc[0] = close
                        h.close = h.close.fillna(method='pad')
                        h.open = h.open.fillna(h.close)
                        h.low = h.low.fillna(h.close)
                        h.high = h.high.fillna(h.close)
                    else:
                        # no prior data. skip ahead to first period with data
                        h = h[h.close.notnull()]

                h = h[:-1]
                h = h.truncate(before=start)
                h = h[['open', 'close', 'high', 'low', 'volume']]
                h['ex_pair_id'] = ep.id

                if not len(h):
                    break

                h.reset_index(inplace=True)
                h['timestamp'] = h.timestamp.apply(lambda d: pd.to_datetime(str(d)))
                log.info(f'New 1H candles: {len(h)}. {h.timestamp.iloc[0]} - {h.timestamp.iloc[-1]}')

                h.to_sql(table_class.__tablename__, engine,
                         index=False, if_exists='append', chunksize=10000,
                         schema='coincube')
                db_session.commit()

                if len(h) <= 1:
                    # incomplete or up to date
                    break
                start = h.timestamp.iloc[-1] + h_delta
                start = start.replace(tzinfo=pytz.UTC)
                close = h.close.iloc[-1]
        except Exception as e:
            log.exception('%s: Unhandled exception' % ep)

    log.info("Finished update.")


if __name__ == '__main__':
    collect_candles_ex()
