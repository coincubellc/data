#!/usr/bin/env python3
from datetime import datetime, timedelta
from pprint import pprint
from common import *


log = logging.getLogger(__name__)
logging.basicConfig(level=logging.DEBUG)

MARKETS_URL = 'https://api.coinmarketcap.com/v2/ticker'

# Get market data from CMC
log.info("Getting tickers from CoinMarketCap...")
resp = requests.get(MARKETS_URL)
markets = resp.json()['data']

assets = {}
for coin_id in markets:
    assets[markets[coin_id]['rank']] = markets[coin_id]

for rank in assets.keys():
    if assets[rank]['symbol'] == 'USDT':
        continue
    cur = Currency.query.filter_by(symbol=assets[rank]['symbol']).first()
    if not cur:
        try: 
            if rank <= 100:
            	log.debug("Adding asset: %s" % assets[rank]['symbol'])
            	asset = Currency(
            		symbol=assets[rank]['symbol'],
                    name=assets[rank]['name'],
            		market_cap=assets[rank]['quotes']['USD']['market_cap'],
                    cmc_id=assets[rank]['website_slug']
            		)
            	db_session.add(asset)
        except:
        	log.debug("Couldn't add: %s" % assets[rank]['symbol'])
db_session.commit()