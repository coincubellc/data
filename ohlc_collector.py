#!/usr/bin/env python3

import calendar
import datetime
import time
from time import sleep
from abc import ABCMeta, abstractmethod
from typing import List, Tuple

import _mysql
import numpy as np
import pandas as pd
import pytz
import requests
from requests import RequestException
from sqlalchemy.exc import IntegrityError

import exapi_client as api
from common import set_logger, Candles1hLocal
from database import ExPair, engine, db_session

table_class = Candles1hLocal  # CombinedExPair1h


GRACE_TIME = 5 # Seconds to sleep on exception
API_RETRIES = 3  # Times to retry query before giving up

KRAKEN_ENDPOINT = 'https://api.kraken.com/0/public/OHLC'
HITBTC_ENDPOINT = "https://api.hitbtc.com/api/2/public/candles/{}?period=H1&limit=5"
BITSTAMP_ENDPOINT = 'https://www.bitstamp.net/api/v2/transactions/{}/'

logger = set_logger(name=__name__, sysloglevel=None, emaillevel=None)


class HourlyCollectorBase(metaclass=ABCMeta):
    """ Base class for all hourly collectors.
    """

    def __init__(self, n_retries=5, retry_delay=30):
        self.retry_delay = retry_delay
        self.n_retries = n_retries
        self.exchange = None
        self.request_interval = 1

        if not table_class.__table__.exists(engine):
            table_class.__table__.create(engine)

    @abstractmethod
    def get_candle_df(self, symbol: str) -> pd.DataFrame:
        ...

    @abstractmethod
    def get_symbol(self, pair: ExPair) -> str:
        ...

    def get_pairs(self) -> List[ExPair]:
        """ Returns the list on pairs available for the exchange.
        """
        pairs = ExPair.query.filter(ExPair.exchange.has(name=self.exchange),
                                    ExPair.active).all()
        return pairs

    def update_all(self):
        """ Updates all pairs on the exchange.
        """

        pairs = self.get_pairs()
        logger.info(f"Started updating symbols on {self.exchange}."
                    f" {len(pairs)} total.")

        results = {pair: False for pair in pairs}

        for i in range(self.n_retries):
            to_retry = [p for p, s in results.items() if not s]
            if i > 0 and len(to_retry):
                # retry to download pairs that failed
                time.sleep(self.retry_delay)
                logger.info("Retry #" + str(i))

            for pair in to_retry:
                try:
                    success = self.update_pair(pair)
                except Exception as e:
                    success = False
                    logger.exception("Exception: ")

                results[pair] = success
                time.sleep(self.request_interval)

        logger.info(f"Finished updating symbols on {self.exchange}. "
                    f"Updated {sum(results.values())} / {len(results)}\n")

    def update_pair(self, pair: ExPair):
        """ Updates one pair.
        """

        logger.info("Updating pair: " + str(pair))
        symbol = self.get_symbol(pair)
        if symbol is None:
            return False

        result = self.get_candle_df(symbol)

        # allow avoiding retries in some scenarios
        # like first update on Bitstamp
        if result is True:
            return True

        if result is None:
            return False

        df = result
        df['ex_pair_id'] = pair.id

        try:
            df.to_sql(table_class.__tablename__, engine,
                      index=False, if_exists='append', schema='coincube')

            db_session.commit()
        # using just one occassionaly fails
        except (_mysql.IntegrityError, IntegrityError):
            s = (f"Candle already present for: "
                 f"{pair.base_currency.name}/{pair.quote_currency.name}"
                 f" @ {pair.exchange.name}. Time: {df.timestamp.iloc[0]}")
            logger.error(s)

        s = df.iloc[0]
        logger.info(f"Added data: T:{s.timestamp} O: {s.open} H: {s.high} L: {s.low} C: {s.close} V: {s.volume}")

        return True

    def drop_partial_bar(self, df: pd.DataFrame) -> pd.DataFrame:
        """ Removes the last (incomplete) candle from the dataframe.
        """
        now = datetime.datetime.utcnow()
        last_ts = df.timestamp.iloc[-1]
        if now.hour == last_ts.hour:
            df = df.iloc[:-1]
        return df

    def get_candle_span(self) -> Tuple[datetime.datetime, datetime.datetime]:
        """ Returns the start, end time for the candle starting the hour before.
        """
        now = datetime.datetime.utcnow().replace(tzinfo=pytz.UTC)
        hour_before = now - datetime.timedelta(seconds=3600)
        hour_start = hour_before.replace(microsecond=0, second=0, minute=0)
        hour_end = hour_start + datetime.timedelta(seconds=3600)
        return hour_start, hour_end

    def trades_to_candle(self, trade_df_list: List[pd.DataFrame]):
        """ Converts a list of transaction dataframes to an OHLCV candle.

        It's necessary to call this during the hour after the candle ends.
        """
        df = pd.concat(trade_df_list)
        df = df.drop_duplicates(subset='tid')
        df = df.set_index(df.timestamp)
        df.sort_index(inplace=True)
        hour_start, hour_end = self.get_candle_span()
        hour_span = np.logical_and(df.index >= hour_start, df.index < hour_end)
        df = df[hour_span]

        data = dict(
            open=df.price.iloc[0],
            high=df.price.max(),
            low=df.price.min(),
            close=df.price.iloc[-1],
            volume=df.amount.sum(),
            timestamp=hour_start.replace(tzinfo=None),
        )
        candle = pd.DataFrame(data, index=[0])
        return candle


class KrakenHourlyCollector(HourlyCollectorBase):
    """ Hourly updater for Kraken exchange.
    """

    def __init__(self):
        super().__init__()
        self.exchange = 'Kraken'
        self.request_interval = 4

    def get_symbol(self, pair):
        s = pair.base_symbol + pair.quote_symbol
        return s

    def get_candle_df(self, symbol):
        for i in range(API_RETRIES + 1):
            try:
                r = requests.get(KRAKEN_ENDPOINT,
                                 params={'pair': symbol, 'interval': 60},
                                 timeout=20)
                if r.status_code == 200:
                    r = r.json()
                elif r.status_code == 502:
                    if i == API_RETRIES:
                        return
                    sleep(GRACE_TIME)
                elif r.status_code == 400:
                    if i == API_RETRIES:
                        return
                    sleep(GRACE_TIME)

            except RequestException as e:
                logger.exception("Request exception")
                return

        if len(r['error']):
            logger.error(f"Error getting data for symbol {symbol}")
            logger.error(r)
            return

        # get the renamed symbol from Kraken
        sym = list(r['result'].keys())[0]

        columns = ['timestamp', 'open', 'high', 'low', 'close', 'vwap', 'volume', 'count']
        df = pd.DataFrame(r['result'][sym], columns=columns)
        df['timestamp'] = pd.to_datetime(df.timestamp, unit='s')
        df = df.drop(['vwap', 'count'], axis=1)

        df = self.drop_partial_bar(df)
        df = df.iloc[-1:]
        return df


class HitBTCHourlyCollector(HourlyCollectorBase):
    """ Hourly updater for HitBTC exchange.
    """

    def __init__(self):
        super().__init__()
        self.exchange = 'HitBTC'
        self.request_interval = 2

    def get_symbol(self, pair: ExPair) -> str:
        return pair.base_currency.symbol + pair.quote_currency.symbol

    def get_candle_df(self, symbol: str) -> pd.DataFrame:
        s = HITBTC_ENDPOINT.format(symbol)
        for i in range(API_RETRIES + 1):
            try:
                r = requests.get(s, timeout=20)
                if r.status_code == 200:
                    r = r.json()
                elif r.status_code == 502:
                    if i == API_RETRIES:
                        return
                    sleep(GRACE_TIME)
                elif r.status_code == 400:
                    if i == API_RETRIES:
                        return
                    sleep(GRACE_TIME)
            except RequestException as e:
                logger.exception("Request exception")
                return

        df = pd.DataFrame(r)
        df['timestamp'] = pd.to_datetime(df.timestamp)
        df = df.rename(columns={'max': 'high', 'min': 'low'})
        df = df.drop(['volumeQuote'], axis=1)

        df = self.drop_partial_bar(df)
        df = df.iloc[-1:]
        return df


class BitstampHourlyCollector(HourlyCollectorBase):
    """ Hourly updater for Bitstamp exchange.

    Bitstamp's candle api endpoint only returns the current (incomplete) hourly bar,
    so with this approach the candle is reconstructed by using transaction data
    for the last hour. The update method should be called at the same time every hour
    so that as few transactions as possible are missed.
    """

    def __init__(self):
        super().__init__()
        self.exchange = '/Bitstamp'
        self.request_interval = 1
        self.previous_data = {}

    def get_symbol(self, pair: ExPair):
        base = pair.base_currency.symbol.lower()
        quote = pair.quote_currency.symbol.lower()
        return base, quote

    def get_candle_df(self, symbol: str):
        """ Method that collects transaction data and builds the hourly candle from that.
        """
        base, quote = symbol

        try:
            h = api.get_history(
                    exchange_name=self.exchange,
                    base=base,
                    quote=quote, 
                    count=0,
                    starttime=0,
                    )
        except Exception as e:
            logger.warning(str(e))
            return None

        df = pd.DataFrame(h)

        previous_data = self.previous_data.get(symbol)
        self.previous_data[symbol] = df.copy()

        # we need more than just transactions for one hour,
        # in order to calculate open, etc.
        if previous_data is None:
            logger.info(f"First update with Bitstamp, so skipping the bar (min 2 are required).")
            return True

        # merge data from the previous request
        df_list = [df, previous_data]

        # handle the edgecase where there's a long delay between updates
        # (for whatever reason), so the candle cannot be built correctly
        prev_update_last = previous_data.timestamp.max()
        current_update_first = df.timestamp.min()
        data_gap = (current_update_first - prev_update_last).seconds
        if current_update_first > prev_update_last and data_gap > 60 * 30:  # > 30 min, no overlap
            logger.error(f"Delay between trades on Bitstamp is too large:"
                         f"{prev_update_last} - {current_update_first}. ({data_gap}) sec."
                         f"Skipping this candle.")
            return True

        candle = self.trades_to_candle(df_list)
        return candle


class GeminiHourlyCollector(HourlyCollectorBase):
    """ Hourly updater for Gemini exchange.
    """

    def __init__(self):
        super().__init__()
        self.exchange = '/Gemini'
        self.request_interval = 1

    def get_symbol(self, pair: ExPair):
        base = pair.base_currency.symbol.lower()
        quote = pair.quote_currency.symbol.lower()
        return base, quote

    def _make_ts(self, dt):
        ts = calendar.timegm(dt.utctimetuple())
        ts = ts.replace(tzinfo=pytz.UTC)
        return ts

    def get_candle_df(self, symbol: str):
        """ Method that collects transaction data (since Gemini doesn't have an OHLC endpoint) and
        builds the hourly candle using that.
        """
        base, quote = symbol
        hour_start, hour_end = self.get_candle_span()

        # collect trades that fall within candle range
        trade_dfs = []

        ts = hour_start.timestamp()
        while True:
            try:
                h = api.get_history(
                        exchange_name=self.exchange,
                        base=base,
                        quote=quote,
                        count=None,
                        starttime=int(ts)
                        )
            except Exception as e:
                logger.warning(str(e))
                return None

            h = pd.DataFrame(h)
            trade_dfs.append(h)

            max_ts = h.timestamp.max().replace(tzinfo=pytz.UTC)

            if max_ts > hour_end:
                break

            ts = max_ts.timestamp()
            time.sleep(self.request_interval)

        candle = self.trades_to_candle(trade_dfs)
        return candle

def collect_ohlc_candles_ex():
    collectors = [BitstampHourlyCollector(), HitBTCHourlyCollector(),
                  KrakenHourlyCollector(), GeminiHourlyCollector()]
                  
    for c in collectors:
        c.update_all()

# simple test update loop. In production, Celery should be used to call the collectors.
if __name__ == '__main__':
    collectors = [
        BitstampHourlyCollector(),
        HitBTCHourlyCollector(),
        KrakenHourlyCollector(),
        GeminiHourlyCollector()
    ]

    for c in collectors:
        c.update_all()
